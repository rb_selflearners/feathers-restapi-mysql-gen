
const Sequelize = require('sequelize');

var exports = module.exports = {};

exports.createAndUseService = function(sequelize, app ) {

    const CopyPost = sequelize.define('posts', {
            content: Sequelize.STRING,
            status: Sequelize.STRING
        },
        {
            timestamps: false,
            underscored: true,
        });

    const service4CopyPost = {
        find(params) {
            let posts = CopyPost.findAll({});
            return Promise.resolve(posts);
        },
        get(id, params) {
        },
        create(data, params) {
        },
        update(id, data, params) {
        },
        patch(id, data, params) {
        },
        remove(id, params) {
        },
        setup(app, path) {
        }
    };

    app.use('/copy_posts', service4CopyPost);
};
