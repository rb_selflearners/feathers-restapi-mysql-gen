
var exports = module.exports = {};

exports.loadServices = (sequelize, app) => {

    const postService =  require('./Services/Post');
    postService.createAndUseService( sequelize, app);

    const copyPostService =  require('./Services/CopyPost');
    copyPostService.createAndUseService( sequelize, app);

    //[#ADDED_MODULES#]

};
